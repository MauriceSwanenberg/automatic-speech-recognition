import math
import os

# import cv2
import matplotlib.pyplot as plt
import numpy as np
# import pandas as pd
import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.keras.layers import (
    Conv1D,
    Dense,
    Dropout,
    Flatten,
    Input,
    MaxPool1D,
)

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
MAX_LENGTH = 16000


def build_model():
    audio = Input(name='audio', shape=(16000, 1))

    x = Conv1D(8, activation='relu', kernel_size=5)(audio)
    x = Conv1D(8, activation='relu', kernel_size=5)(x)
    x = MaxPool1D()(x)
    x = Conv1D(8, activation='relu', kernel_size=5)(x)
    x = Conv1D(8, activation='relu', kernel_size=5)(x)
    x = MaxPool1D()(x)
    x = Conv1D(8, activation='relu', kernel_size=5)(x)
    x = Conv1D(8, activation='relu', kernel_size=5)(x)
    x = MaxPool1D()(x)
    x = Conv1D(16, activation='relu', kernel_size=5)(x)
    x = Conv1D(16, activation='relu', kernel_size=5)(x)
    x = MaxPool1D()(x)
    x = Conv1D(16, activation='relu', kernel_size=5)(x)
    x = Conv1D(16, activation='relu', kernel_size=5)(x)
    x = MaxPool1D()(x)
    x = Conv1D(32, activation='relu', kernel_size=5)(x)
    x = Conv1D(32, activation='relu', kernel_size=5)(x)
    x = MaxPool1D()(x)
    x = Conv1D(32, activation='relu', kernel_size=5)(x)
    x = Conv1D(32, activation='relu', kernel_size=5)(x)
    x = MaxPool1D()(x)
    x = Conv1D(32, activation='relu', kernel_size=5)(x)
    x = Conv1D(32, activation='relu', kernel_size=5)(x)
    x = MaxPool1D()(x)
    x = Conv1D(64, activation='relu', kernel_size=5)(x)
    x = Conv1D(64, activation='relu', kernel_size=5)(x)
    x = MaxPool1D()(x)
    x = Conv1D(64, activation='relu', kernel_size=5)(x)
    x = Conv1D(64, activation='relu', kernel_size=5)(x)
    x = Flatten()(x)
    x = Dropout(0.25)(x)
    x = Dense(512, activation='relu')(x)
    x = Dropout(0.25)(x)
    keyword = Dense(12, activation='softmax', name='label')(x)

    return tf.keras.Model(inputs=audio, outputs=keyword)


def pad_audio(audio, label):
    return tf.pad(audio, ((0, MAX_LENGTH - len(audio)),)), label


def expand(audio, label):
    return tf.expand_dims(audio, axis=-1), label


def one_hot_enc(audio, label):
    return audio, tf.one_hot(label, 12)


def plot_time_series(data):
    plt.rcParams.update({'font.size': 22})
    plt.figure(figsize=(14, 8))
    plt.ylabel('Maximum difference in amplitude')
    plt.xlabel('Cumulative frequency')
    plt.plot(np.linspace(0, len(data), len(data)), data)
    plt.show()


def plot_acc(acc_list):
    plt.rcParams.update({'font.size': 22, 'legend.fontsize': 22})
    plt.figure(figsize=(14, 8))
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    for i in acc_list:
        plt.plot(i[0].values, label=i[1])
    plt.tight_layout()
    plt.legend()
    plt.show()


def plot_loss(acc_list):
    plt.rcParams.update({'font.size': 22, 'legend.fontsize': 22})
    plt.figure(figsize=(14, 8))
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    for i in acc_list:
        plt.plot(i[0].values, label=i[1])
    plt.tight_layout()
    plt.legend()
    plt.show()


def white_noise(audio, label):
    return audio + tf.expand_dims(
        tf.constant(
            np.random.normal(scale=.02, size=MAX_LENGTH).astype(np.float64),
            ), axis=-1,
        ), label


def speed_tuning(audio):
    n_samples = len(audio)
    speed_rate = np.random.uniform(0.7, 1.3)
    audio = np.interp(
        np.linspace(0, n_samples, int(speed_rate * n_samples), endpoint=False),
        range(n_samples),
        audio,
    )
    n_samples_new = len(audio)
    if n_samples_new > n_samples:
        diff = n_samples_new - n_samples
        audio = audio[math.floor(diff / 2):-math.ceil(diff / 2)]
    return tf.cast(tf.convert_to_tensor(audio), tf.int64)


def speed_tuning_wrapper(audio, label):
    audio = tf.py_function(speed_tuning, [audio], tf.int64)
    return audio, label


def normalise(audio, label):
    return audio/tf.reduce_max(audio), label


dataset = tfds.load('speech_commands', data_dir='data', as_supervised=True)
train_dataset = dataset['train'].map(normalise).map(pad_audio).map(expand)
train_dataset = train_dataset.map(one_hot_enc).map(white_noise).shuffle(256)
validation_dataset = dataset['validation'].map(normalise).map(pad_audio).map(expand)
validation_dataset = validation_dataset.map(one_hot_enc)
test_dataset = dataset['test'].map(normalise).map(pad_audio).map(expand).map(one_hot_enc)

model = build_model()

model.compile(
    loss=tf.keras.losses.CategoricalCrossentropy(),
    metrics=['accuracy'],
    optimizer='adam',
)

reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(
    monitor='val_accuracy',
    factor=0.4,
    patience=4,
    verbose=1,
    min_lr=0.000005,
    min_delta=0.0015,
)

model.fit(
    train_dataset.batch(256).prefetch(2),
    validation_data=validation_dataset.batch(256).prefetch(2),
    epochs=60,
    verbose=1,
    callbacks=[reduce_lr, tf.keras.callbacks.TensorBoard(log_dir='data/logs')],
)
model.save('model_02noise_normalised.hdf5')
